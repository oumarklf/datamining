**ANALYSE**

Suite à la préparation et la visualisation des données, nous avons décidé de réaliser une classification par arbre de décision de la variable DIED afin de prédir si un patient a des risques élevé de mourir selon ses caractéristique. Cela permetrait donc de le prendre en charge en priorité et d'utiliser les ressources de santé avec efficacité.

Nous entrainons donc dans un premier temps l'arbre de décision sur deux tiers des valeurs du jeu de données pour ensuite le tester sur le tier restant.
Nous passons ensuite à l'étape de prédiction des classes sur l'ensemble des test.
Enfin nous avons évalué les performances de notre classification à l'aide d'une matrice de confusion, la precision, le recall, le f1-score, le support et enfin en traçant la courbe ROC.