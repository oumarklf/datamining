# Page d'accueil


## Étude sur la prédiction de la mortalité due au COVID-19

Notre projet s'inscrit dans le contexte de la pandémie de COVID-19, l'une des crises sanitaires les plus importantes du XXIe siècle, ayant touché des millions de personnes à travers le monde. Nous avons utilisé des données gouvernementales mexicaines pour explorer les caractéristiques des patients atteints de COVID-19 et leur corrélation avec le risque de décès.  
 À travers des techniques d'apprentissage supervisé, nous avons développé un modèle de prédiction du risque de décès des patients.

Consultez le rapport complet pour explorer les données, les techniques utilisées et les résultats obtenus. 

[Rapport complet](https://gitlab.com/oumarklf/datamining/-/blob/main/rapport/README.md?ref_type=heads)