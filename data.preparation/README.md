
**Pré-traitement**

Après avoir récupéré le fichier Covid_Data.csv, nous avons installé le package "tidyverse", regroupant plusieurs packages R utiles pour la manipulation et l'analyse de données. À l'aide de ce package, nous avons exploré les variables potentiellement intéressantes en fonction du nombre de modalités qu'elles contiennent, en utilisant la fonction summary().

Au cours de cette exploration, nous avons remarqué que certaines variables présentaient des valeurs manquantes. Pour la variable DATE_DIED, qui contient la date de décès des patients ou 9999-99-99 s'ils sont toujours en vie, nous l'avons transformée en variable numérique : attribuant la valeur 1 aux patients vivants et 2 aux patients décédés. En ce qui concerne la variable PREGNANT, nous avons observé que tous les hommes étaient classés en valeurs manquantes (97). Nous avons donc remplacé ces valeurs par 2, indiquant qu'ils ne sont pas enceints. De même, pour les 3754 femmes avec des valeurs manquantes, nous avons attribué la valeur 2.

**Gestion des valeurs manquantes** 

Dans l'ensemble, notre jeu de données présentait des valeurs manquantes représentées par les chiffres 97, 98 ou 99, ce qui compliquait leur traitement. Par conséquent, nous avons décidé de les transformer en valeurs NA. En examinant le pourcentage de valeurs manquantes par variable, nous avons constaté que les variables INTUBED et ICU présentaient plus de 80 % de valeurs manquantes. Nous avons donc pris la décision de les supprimer de notre jeu de données.

Nous avons également remarqué que le pourcentage de valeurs manquantes dans l'ensemble du jeu de données ne représentait que 5 %. Par conséquent, nous avons choisi de supprimer toutes les valeurs manquantes lors de leur gestion.

Une fois notre jeu de données nettoyé et notre matrice individus/variables complétée, nous l'avons enregistré dans le fichier data_clean.csv pour une utilisation ultérieure dans notre analyse.
Cependant ce jeu de données nous laissait perplexe car il était composé à 93% de patients vivants
De ce fait, nous avons également enregistré un fichier data_clean_2.csv qui contient 50% de patients décédés et 50% de patients vivants échantillonnées aléatoirement

**Visualisation**

Nous avons créé une heatmap de corrélation pour observer les relations entre toutes les variables du jeu de données. Nous avons ensuite réalisé un histogramme de la répartition des âges des patients, notant que la majorité se situait entre 20 et 60 ans. Enfin, nous avons généré des histogrammes des différentes variables en fonction de la variable DIED, mettant en évidence certaines tendances telles que le nombre élevé de décès chez les patients hospitalisés par rapport à ceux rentrant chez eux, l'égalité des décès entre hommes et femmes, ainsi que la prévalence des décès chez les patients plus âgés et ceux atteints de certaines conditions médicales telles que les pneumonies, le diabète, l'hypertension et l'obésité. De plus, nous avons observé que les patients classés avec un niveau de gravité du covid 3 étaient plus susceptibles de décéder que les autres.