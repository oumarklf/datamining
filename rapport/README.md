# Contexte

Identifié en Chine en janvier 2020, le coronavirus nommé SARS-CoV-2 et dont la maladie caractéristique est le COVID-19, est à l'origine de la pandémie la plus meurtrière du XXIe siècle avec près de 7 millions de victimes réparties dans le monde entier.  
L'infection par ce virus entraîne une l'apparition de symptômes caractéristiques tels que : la fièvre, une toux ou un essoufflement, des maux de tête, une fatigue intense, une perte d'odorat, une disparition du goût et une diarrhée. Dans la plupart des infections, la maladie ne nécessite pas de traitement particulier et disparaît après quelques jours de repos. Cependant, dans certains cas, notamment chez les personnes faibles (personnes âgées ou avec d’autres problèmes de santé), le COVID-19 peut entraîner des complications et la maladie peut devenir létale. 

![Hospitalisation COVID](graphCOVID.PNG)

Dans le cadre de notre projet, nous allons nous placer au cœur la pandémie, période durant laquelle les services de santé du monde entier se sont retrouvés débordés et ou la prise en charge des personnes atteintes du COVID-19 est devenue une priorité majeure. Dans cette optique, nous avons essayé à partir d’un jeu de données sur des patients souffrant du COVID-19, notre objectif était de détecter les patients avec un fort risque de développer une forme grave de la maladie pour les prendre en charge plus rapidement et efficacement.  
Nous nous sommes donc concentrés sur la corrélation entre certaines des caractéristiques des patients et leur probabilité de décès due au COVID-19 ?


Les données sur lesquelles se base notre projet proviennent du gouvernement mexicain (https://datos.gob.mx/busca/dataset/informacion-referente-a-casos-covid-19-en-mexico). L'ensemble de données comprend 21 variables uniques pour 1 048 576 patients uniques, les variables représentent des informations telles que : l’âge, le sexe, la mort ou non du patient et la présence d’antécédents médicaux de divers maladie comme le diabète, une pneumonie, l’asthme, l’hypertension… (cf data/)


# Analyse

Notre objectif principal est de développer un modèle de classification capable de prédire avec précision si un patient est à haut risque de décès dû au COVID-19. Pour y parvenir, nous avons plusieurs sous-objectifs :
 * Collecte et préparation des données : 
Nous devons d'abord acquérir un ensemble de données fiable et complet sur les patients COVID-19, comprenant des informations telles que l'âge, le sexe, les conditions médicales préexistantes et le statut de survie.
Concrètement, nous disposions d'une variable indiquant la date de décès des patients, ce qui nous permettait de déterminer leur statut vital (décédé ou non). 
  * Exploration des données : 
Après avoir récupéré nos données, on les visualise en utilisant les outils disponibles sur Rstudio afin de comprendre la structure des données, identifier les tendances, les relations entre les variables et les éventuelles valeurs aberrantes ou manquantes.
  * Sélection des variables :
Suite à cela, nous sélectionnerons les variables les plus pertinentes pour la prédiction du décès des patients. Ainsi, on enlèvera des variables en fonction de leurs corrélations à notre variable d’intérêt.
  * Modélisation : 
Nous utiliserons des techniques d'apprentissage supervisé.
En raison de la nature binaire de notre variable cible, nous avons opté pour une approche basée sur les arbres de décision, 
  * Évaluation du modèle :
Une fois le modèle construit, nous évaluerons ses performances en utilisant des métriques telles que la précision, la sensibilité, le score F1 et l'aire sous la courbe ROC (AUC-ROC). Cela nous permettra de déterminer à quel point notre modèle est capable de généraliser sur de nouvelles données.

# Conception

Nous avons choisi l’approche des arbres de décision en raison de sa capacité à gérer efficacement les données complexes, sa flexibilité et son interprétabilité, ce qui en fait une méthode appropriée pour notre problème de prédiction du risque de décès lié au COVID-19.
Afin de réaliser notre modèle prédictif du statut des patients, nous sommes passés par 3 étapes qui sont :
  * Obtention de la matrice individus-variables+classes :
Dans cette phase, nous sélectionnons les variables les plus pertinentes et les transformons si nécessaire pour créer une matrice individus-variables utilisable. Nous nettoyons les données en traitant les valeurs manquantes et en supprimant les variables inutiles. Une fois ces étapes terminées, nous obtenons une matrice où chaque ligne représente un patient, chaque colonne représente une caractéristique, et la classe indique si le patient est décédé ou non.
  * Utilisation de la matrice pour produire des résultats : 
Avec notre matrice préparée, nous passons à la phase d'utilisation pour produire des résultats. Nous appliquons des méthodes de classification, en particulier des techniques basées sur les arbres de décision, pour prédire le décès ou non des patients en fonction de leurs caractéristiques.
  * Évaluation des résultats obtenus
Une fois le modèle entraîné, nous évaluons ses performances en utilisant des mesures telles que la précision, le rappel et le F1-score. En analysant ces mesures, nous pouvons déterminer l'efficacité du modèle dans la prédiction du décès des patients. De plus, nous tracions la courbe de ROC pour évaluer la performance globale du modèle dans sa capacité à discriminer entre les classes.

# Réalisation

Pour concrétiser notre analyse, nous avons d'abord nettoyé notre jeu de données en transformant la variable DATE_DIED en une variable numérique. Nous avons attribué la valeur 1 aux patients vivants et 2 aux patients décédés. De même, nous avons géré les valeurs manquantes de la variable PREGNANT en les remplaçant par 2 pour les hommes et les femmes, car c'était la modalité majoritaire.    

Nous avons également converti toutes les valeurs représentant des données manquantes en valeurs NA et supprimé les variables présentant trop de valeurs manquantes. Enfin, nous avons choisi de supprimer toutes les valeurs manquantes dans l'ensemble du jeu de données, représentant seulement 5 % de notre jeu de données. Une fois ces étapes achevées, nous avons obtenu une matrice de données nettoyée et complétée, enregistrée sous le nom de "data_clean.csv" pour une utilisation ultérieure dans notre analyse. Mais aussi une matrice data_clean_2.csv qui est plus équilibrée.  

En utilisant des bibliothèques telles que ggplot et tidyverse, nous avons créé des graphiques permettant de visualiser les corrélations des variables avec notre classe.     

Nous avons utilisé Jupyter Lab pour créer un arbre de décision afin de classifier les individus en fonction de leurs attributs. Pour cela, nous avons importé les modules pandas et Sklearn, qui fournissent de nombreux outils utiles à la classification.    

Nous avons divisé nos données initiales en un jeu de données d'apprentissage et un jeu de données de test, en utilisant les deux tiers pour l'apprentissage et en conservant le tiers restant pour l'évaluation de la performance du modèle. Cette approche nous a permis d'éviter le surapprentissage et de garantir la généralisation du modèle à de nouvelles données, assurant ainsi sa capacité à bien se comporter sur des données non vues précédemment.   

Pour évaluer les performances de notre modèle, nous avons utilisé des mesures telles que la précision, la sensibilité et le F1-score.     

Pour ce faire, nous avons testé le modèle obtenu sur le tiers restant du jeu de données afin d'obtenir une matrice de confusion.  

À partir de cette matrice, nous avons calculé la précision, la sensibilité et le F1-score. En outre, nous avons tracé la courbe de ROC (receiver operating characteristic) en utilisant ces paramètres pour évaluer la performance globale de notre modèle et déterminer s'il était significatif dans sa capacité à prédire avec précision la classe des individus.

# Discussion

![Resultats modele 1](modele1.PNG)

Sur notre premier modèle réalisé, on a remarqué que les patients vivants étaient prédits avec une précision de 96% tandis que les prédictions sur les patients décédés étaient de 54%. Il semblerait donc que le modèle ait une meilleure capacité à identifier les individus qui survivront que ceux qui décéderont.  

Cette précision juste acceptable pour les patients décédés peut s’expliquer de la grande différence de quantité de données. En effet, nous avons 74.612 patients décédés tandis qu’on a 950.217 vivants, soit 92% du jeu de données.   

Pour palier à ce problème, on a réalisé un autre dataframe composé à 50% de patients vivants et 50% de patients décédés.   
Après avoir refait la démarche, on obtient ces résultats :

![Resultats modele 2](modele2.PNG)

Sur ce nouveau modèle, les patients vivants et les patients décédés sont tout aussi bien prédit avec des valeurs de précision, de recall, de spécificité et de f1 score proche de 88%.  

Ainsi, le fait de prendre un jeu de données plus équilibré nous a permis de résoudre notre problème même si on perd sûrement en précision vu qu’on prend beaucoup moins de données.  
La précision de notre modèle sur le jeu de test est de  87.9%

![Precision modele](precisionModele.PNG)

Ainsi le taux d’erreur est de 12.1%

![Taux erreur modele](tauxErreur.PNG)

Nous observons une courbe ROC remarquable, avec une aire sous la courbe de 0,89. Cela démontre que notre modèle est bien plus significatif qu'un modèle généré de manière aléatoire.

![Courbe ROC](courbeROC.PNG)


# Bilan et perspectives

Notre modèle de classification basé sur l'arbre de décision a démontré une précision notable de 88%. Cela atteste de l'efficacité de notre approche dans la classification des patients en fonction de leurs attributs médicaux.  

Bien que notre modèle actuel ait donné des résultats prometteurs, il existe des possibilités d'amélioration. En particulier, l'élagage de l'arbre de décision pourrait être exploré pour éviter le surapprentissage et améliorer la généralisation du modèle.     

Notre approche méthodologique a été solide, avec une attention particulière portée au prétraitement des données et à la validation du modèle.  

Cependant, nous avons rencontré des défis dans la gestion des valeurs manquantes et dans la sélection des variables les plus pertinentes pour la classification. Ces défis nous ont permis de mieux comprendre les subtilités des données médicales et l'importance d'une analyse approfondie surtout avec des variables de classes équilibrées.     

Pour les futures recherches, nous envisageons d'explorer davantage les techniques d'élagage des arbres de décision pour améliorer la performance de notre modèle tout en réduisant sa complexité.  
 De plus, l'exploration du modèle de forêts aléatoires avec le bootstrap aggregating pourrait offrir une perspective intéressante pour renforcer la robustesse de nos prédictions. Enfin, l'application de notre modèle à d'autres ensembles de données médicales pourrait permettre de valider sa généralisabilité et son utilité dans d'autres contextes cliniques.


# Gestion du projet

L’organisation lors de la réalisation de ce projet s’est basée sur une communication fréquente à propos de toutes les étapes à réaliser et des problèmes rencontrés.  
Nous avons consacré beaucoup de temps à la recherche du jeu de données, car nous avons rencontré de nombreux échecs et avons eu du mal à trouver un jeu de données intéressant qui convenait à nos capacités d’analyse. Nous avons donc effectué des analyses préliminaires sur plusieurs jeux de données avant de trouver celui qui répondait à nos critères.     

Le travail sur ce projet s’est déroulé en alternant entre des sessions en présentiel dans la salle U2-207 pour les premières étapes du projet et des sessions à distance pour la suite.

![Diagramme de GANTT](Gantt.PNG)
