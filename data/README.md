Données récupérés sur kaggle via le lien : https://www.kaggle.com/datasets/meirnizri/covid19-dataset

**Contexte**

Le COVID-19 est une maladie infectieuse causée par un coronavirus. La plupart des personnes infectées par le virus COVID-19 souffre d’une maladie respiratoire légère à modérée et se rétablissent sans traitement spécifique. Les personnes âgées et celles souffrant de problèmes médicaux comme les maladies cardiovasculaires, le diabète, les maladies respiratoires et le cancer sont plus susceptibles de développer une maladie grave.

Tout au long de la pandémie, l’un des principaux problèmes auxquels les services de santé ont été confrontés est le manque de ressources médicales. En ces temps difficiles, être capable de prédire le type de ressources dont un individu pourrait avoir besoin sera d'une immense aide pour les autorités car elles pourraient se procurer et organiser les ressources nécessaires pour sauver la vie de ce patient.

**Données**

L'ensemble de données a été fourni par le gouvernement mexicain (https://datos.gob.mx/busca/dataset/informacion-referente-a-casos-covid-19-en-mexico). L'ensemble de données comprend 21 variables uniques pour 1 048 576 patients uniques . Dans les fonctionnalités booléennes, 1 signifie « oui » et 2 signifie « non ». les valeurs telles que 97 et 99 sont des données manquantes .


- USMER : Indique si le patient a traité dans des unités médicales de premier, deuxième ou troisième niveau.

- UNITE_MÉDICALE : type d’institution du système national de santé qui a dispensé les soins.

- SEXE : 1 - femelle. 2 - mâle

- PATIENT_TYPE : type de soins que le patient a reçus dans l'unité. 1 pour retour à domicile et 2 pour hospitalisation.

- DATE_DIED : Si le patient est décédé indiquer la date du décès, et 9999-99-99 sinon.

- INTUBED si le patient était connecté au respirateur.

- PNEUMONIE : si le patient présente déjà une inflammation des poumons ou non.

- ÂGE : âge du patient.

- ENCEINTE : si la patiente est enceinte ou non.

- DIABÈTE : si le patient est diabétique ou non.

- BPCO : si le patient est atteint ou non d'une maladie pulmonaire obstructive chronique.

- ASTHME : si le patient souffre d'asthme ou non.

- INMSUPR : si le patient est immunodéprimé ou non.

- HIPERTENSION : si le patient souffre d'hypertension ou non.

- OTHER_DISEASE : si le patient a une autre maladie ou non.

- CARDIOVASCULAIRE : si le patient souffre d'une maladie liée au cœur ou aux vaisseaux sanguins.

- OBÉSITÉ: si le patient est obèse ou non.

- RENAL_CHRONIQUE : si le patient souffre d'une maladie rénale chronique ou non.

- TOBACCO : si le patient est un fumeur.

- CLASIFFICATION_FINAL : Résultats des tests Covid.

- USI : si le patient a été admis dans une unité de soins intensifs



